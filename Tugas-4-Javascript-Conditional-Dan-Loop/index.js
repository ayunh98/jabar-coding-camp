// Soal 1
console.log("Soal 1");
var nilai = 66;
if (nilai >= 85) {
    console.log("A");
    } else if (nilai < 85 && nilai >= 75) {
        console.log("B");
    } else if (nilai < 75 && nilai >= 65) {
        console.log("C");
    } else if (nilai < 65 && nilai >= 55) {
        console.log("D");
    }
else {
    console.log("E");
}


// Soal 2
console.log("Soal 2");
var tanggal = 9;
var bulan = 3;
var tahun = 1998;
switch (bulan) {
    case 1:   { console.log(tanggal, 'Januari', tahun); break; }
    case 2:   { console.log(tanggal, 'Februari', tahun); break; }
    case 3:   { console.log(tanggal, 'Maret', tahun); break; }
    case 4:   { console.log(tanggal, 'April', tahun); break; }
    case 5:   { console.log(tanggal, 'Mei', tahun); break; }
    case 6:   { console.log(tanggal, 'Juni', tahun); break; }
    case 7:   { console.log(tanggal, 'Juli', tahun); break; }
    case 8:   { console.log(tanggal, 'Agustus', tahun); break; }
    case 9:   { console.log(tanggal, 'September', tahun); break; }
    case 10:  { console.log(tanggal, 'Oktober', tahun); break; }
    case 11:  { console.log(tanggal, 'November', tahun); break; }
    case 12:  { console.log(tanggal, 'Desember', tahun); break; }
    default:  { console.log('Tidak Valid'); }} 

    
// Soal 3
console.log("Soal 3");
var n = 3; 
for (i = 1; i <= n; i += 1) {
    var hasil = "";
    for (j = 1; j<= i; j += 1) {
        hasil+="#";
        }
    console.log(hasil);
    }


// Soal 4
console.log("Soal 4");
var n = 10;
 

for (i = 1; i <=n; i++) {
    var a = "Programming";
    var b = "Javascript";
    var c = "VueJS";
    var pemisah = "";
    if (((i + 2) % 3 == 0) || (i % 3 == 1)) {
        console.log(i, "- I love", a);
    } else if (((i + 1) % 3 == 0) || (i % 3 == 2)) {
        console.log(i, "- I love", b);
    } else if ((i % 3 == 0) || (i % 3 == 3)) {
        console.log(i, "- I love", c);
        for (j = 1; j<= i; j += 1) {
            pemisah+="=";
            }
        console.log(pemisah);
    }
    
}