// Soal 1
console.log("Soal 1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for (var i = 0;  i < 5; i++) {
    console.log(daftarHewan[i]);
}


// Soal 2
console.log("Soal 2");
function introduce(name, age, address, hobby) {
    return ("Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby);
    }     
   
var data = {name : "John", age : 30, address : "Jalan Pelesiran", hobby : "Gaming"};
 
var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


// Soal 3
console.log("Soal 3");
function hitung_huruf_vokal(huruf) {
    var kecil = huruf.toLowerCase();
    var abjad = kecil.split("");
    var n = huruf.length;
    var vokal = ["a", "i", "u", "e", "o"];
    var hasil = 0; 
    for (var i = 0; i < n; i++) {
        var urutan = abjad[i];
        if (urutan == "a") {
            var angka = 1;
        } else if (urutan == "i") {
            angka = 1;
        } else if (urutan == "u") {
            angka = 1;
        } else if (urutan == "e") {
            angka = 1;
        } else if (urutan == "o") {
            angka = 1;
        } else  {
            angka = 0;
        }
        hasil = hasil+angka;
    }    
    return hasil
}

var hitung_1 = hitung_huruf_vokal("Ayu")
var hitung_2 = hitung_huruf_vokal("Nurhasanah")

console.log(hitung_1 , hitung_2) // 3 2


// Soal 4
console.log("Soal 4");
function hitung(angka) {
    var hasil = angka + angka - 2
    return hasil
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8