// Soal 1
console.log("Soal 1");

const luas = (p,l) => {
    let luaspp = p*l
    console.log(`Luas persegi panjang dengan panjang ${p} dan lebar ${l} adalah ${luaspp}`)
}

const keliling = (p,l) => {
    let kelpp = 2*(p+l)
    console.log(`Keliling persegi panjang dengan panjang ${p} dan lebar ${l} adalah ${kelpp}`)
}

luas(3,4)
keliling(3,4)


// Soal 2
console.log("Soal 2");
  
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName() {
          console.log(firstName, lastName)
        }
      }  
}

newFunction("William", "Imoh").fullName()


// Soal 3
console.log("Soal 3");

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)


// Soal 4
console.log("Soal 4");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]

console.log(combined)


// Soal 5
console.log("Soal 5");

const planet = "earth" 
const view = "glass" 

var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}` 

console.log(before)