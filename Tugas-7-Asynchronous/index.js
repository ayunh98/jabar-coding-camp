// Soal 1
console.log("Soal 1")

// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
readBooks(10000, books[0], function (a1) {
    readBooks(7000, books[1], function (a2) {
        readBooks(5000, books[2], function (a3) {
            readBooks(1000, books[3], function (a4) {              
            })
        })
    })
})